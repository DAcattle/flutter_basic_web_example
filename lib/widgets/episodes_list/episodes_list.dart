import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:provider/provider.dart';
import 'package:the_basics/routing/route_names.dart';
import 'package:the_basics/view_models/episode_view_model.dart';

import 'episode_item.dart';

class EpisodesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var episodes = context.watch<EpisodeViewModel>().episodes;

    return episodes.isEmpty
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Wrap(
            spacing: 30,
            runSpacing: 30,
            children: <Widget>[
              ...episodes
                  .asMap()
                  .map((index, episode) => MapEntry(
                        index,
                        GestureDetector(
                          onTap: () {
                            Modular.to.navigate('/episodes/detail/$index');
                          },
                          child: EpisodeItem(model: episode),
                        ),
                      ))
                  .values
                  .toList()
            ],
          );
  }
}
