import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:the_basics/view_models/episode_details_view.dart';
import 'package:provider/provider.dart';

class EpisodeDetails extends StatefulWidget {
  final int id;

  const EpisodeDetails({Key key, this.id}) : super(key: key);

  @override
  _EpisodeDetailsState createState() => _EpisodeDetailsState();
}

class _EpisodeDetailsState extends State<EpisodeDetails> {
  final model = EpisodeDetailsViewModel();
  final test = [];

  @override
  void initState() {
    print(widget.id);
    model.getEpisodeData(widget.id.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<EpisodeDetailsViewModel>.value(
      value: model,
      child:
          Consumer<EpisodeDetailsViewModel>(builder: (context, value, child) {
        final episode = value.episode;
        return Center(
          child: Column(
            children: <Widget>[
              episode == null
                  ? Container()
                  : Container(
                      height: 320,
                      child: RouterOutlet(),
                    ),
              episode == null
                  ? CircularProgressIndicator()
                  : Text(
                      episode.title,
                      style: TextStyle(fontSize: 60),
                    ),
              episode == null
                  ? Container()
                  : ElevatedButton(
                      onPressed: () =>
                          Modular.to.navigate('/episodes/detail/${widget.id}/ep2'),
                      child: Text('Next'),
                    )
            ],
          ),
        );
      }),
    );
  }
}

class PageA extends StatelessWidget {
  final String imageUrl;

  const PageA({this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.redAccent,
      height: 320,
      width: 500,
    );
  }
}

class PageB extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueAccent,
      height: 320,
      width: 500,
    );
  }
}
