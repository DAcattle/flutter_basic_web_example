import 'package:flutter/material.dart';
import 'package:the_basics/modules/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'locator.dart';

void main() {
  setupLocator();
  runApp(ModularApp(
    module: AppModule(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: Theme.of(context).textTheme.apply(fontFamily: 'Open Sans'),
      ),
    ).modular();
  }
}
