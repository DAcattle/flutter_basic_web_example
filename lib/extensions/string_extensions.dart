import 'package:the_basics/datamodels/routing_data.dart';

extension StringExtension on String {
  RoutingData get getRoutingData {
    var uriData = Uri.parse(this);
    print('this: $this queryParameters: ${uriData.queryParameters} path: ${uriData.path}');
    print('split: ${Uri.splitQueryString(uriData.path)}');
    return RoutingData(
      queryParameters: uriData.queryParameters,
      route: uriData.path,
    );
  }
}
