import 'package:flutter_modular/flutter_modular.dart';
import 'package:the_basics/modules/app_guard.dart';
import 'package:the_basics/modules/episode_module.dart';
import 'package:the_basics/routing/route_names.dart';
import 'package:the_basics/view_models/episode_view_model.dart';
import 'package:the_basics/views/about/about_view.dart';
import 'package:the_basics/views/episodes/episodes_view.dart';
import 'package:the_basics/views/home/home_view.dart';
import 'package:the_basics/views/layout_template/layout_template.dart';

class AppModule extends Module {
  @override
  List<ModularRoute> get routes => [
        ChildRoute('/',
            child: (_, args) => LayoutTemplate(),
            children: [
              ChildRoute('/', child: (_, args) => HomeView()),
              ModuleRoute(EpisodesRoute, module: EpisodeModule()),
              ChildRoute(
                AboutRoute,
                child: (_, args) => AboutView(),
              ),
            ],
            guards: [AppGuard()],
            guardedRoute: '/'),
      ];
}
