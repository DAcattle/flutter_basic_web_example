import 'package:flutter_modular/flutter_modular.dart';

class AppGuard implements RouteGuard {
  @override
  Future<bool> canActivate(String path, ModularRoute router) async {
    print(path);
    if (path == '/about') {
      return true;
    } else
      return false;
  }

  @override
  // TODO: implement guardedRoute
  String get guardedRoute => throw UnimplementedError();
}